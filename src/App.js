import React, { Component } from 'react';
import { BrowserRouter as Router , withRouter} from 'react-router-dom';
import Route from 'react-router-dom/Route';
import './App.css';
import {Provider } from 'react-redux';
import { Select } from 'antd';
import ImageFeeds from './components/ImageFeeds'
import TextFeeds from './components/TextFeeds'
import AllFeeds from "./components/AllFeeds";
import store from './store';
const Option = Select.Option;


class DropDown extends Component {
  onChange = (e) => {
    this.props.history.push(`/${e}`);
  }
  render() {
    return (
     
      <Select
      showSearch
      style={{ width: 400 }}
      placeholder="Select Type"
      onChange={this.onChange}
      >
      <Option value="text">Text Only</Option>
      <Option value="image">Image Only</Option>
      <Option value="imageText">Image and Text</Option>
      <Option value="noItem">No Item</Option>
    </Select>
    );
  }
}

const Menu = withRouter(DropDown);

class App extends Component {
  render() {
    return (
      <Provider store = {store}>

      <Router>
      <div className="App">
      <Menu />
  
      <Route
        path="/image"
        exact
        component={ImageFeeds} />

     <Route
      path="/text"
      exact
      component={TextFeeds} />

    <Route
      path="/imageText"
      exact
      component={AllFeeds}  />
      </div>
      </Router>
      </Provider>
    );
  }
}

export default App;

import React, { Component } from 'react';
import '../App.css';
import CommentContainer from './comment/CommentContainer';
import Feeds from './feed/Feeds' ;
import {connect} from 'react-redux'
import { post } from '../actions/PostAction'

class AllFeeds extends Component {
  componentWillMount(){
    this.props.post()
    }  
    
    render() {
      console.log(this.props.data,"All FEEDS")
      const dt=  this.props.data.map((data,index)=>(
          <div key = {Math.random()}>
          <Feeds item_description={data.item_description} count={data.likes} id = {index} />
          <CommentContainer comment={data.comments} id = {index}/> 
          </div>
       ))
    return (
      <div className="App">
      <h2>----Image----</h2>
      { dt }
      </div>
    );
  }
}
const mapStateToProps = (state)=>({
  data:state.post.items
  })

export default connect(mapStateToProps,{post})(AllFeeds);
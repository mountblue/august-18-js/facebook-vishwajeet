import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Badge, Button, } from 'antd';
import '../../App.css';
import { LikeFeed } from '../../actions/PostAction';

class Feed extends Component {

  increase = () => {
       this.props.LikeFeed(this.props.id)
  }
  
  render() {
    let button = <Badge count={this.props.count}><Button type="Primary" onClick={this.increase}>Like</Button></Badge>;
    if(this.props.likesArray.includes(this.props.id))
      button = <Badge count={this.props.count}><Button type="Primary" onClick={this.increase}>Unlike</Button></Badge>;
    return (
      <div>
        <div>
      <h2>
        {this.props.item_description}
      </h2>
      </div>
        {button}
           </div>
      
    );
  }
}


const mapStateToProps = state => ({
  likesArray: state.post.Likes,
  
}); 
export default connect(mapStateToProps, { LikeFeed })(Feed);

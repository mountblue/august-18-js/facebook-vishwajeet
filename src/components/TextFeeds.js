import React, { Component } from 'react';
import '../App.css';
import CommentContainer from './comment/CommentContainer';
import Feeds from './feed/Feeds'
import {connect} from 'react-redux'
import { post } from '../actions/PostAction'

class TextFeeds extends Component {
  componentWillMount(){
    this.props.post()
    }  
      
    render() {
      const dt=  this.props.data.filter(el=> el.image === "").map(data=>(
          <div key = {Math.random()}>
          <Feeds item_description={data.item_description} count={data.likes} id = {0}/>
          <CommentContainer comment={data.comments} id = {0}/> 
          </div>
       ))
       return (
        <div className="App">
        <h2>----Text Only----</h2>
      
          { dt } 
        </div>
      );
  }
}
const mapStateToProps = (state)=>({
  data:state.post.items
  })
  
  export default connect(mapStateToProps,{post})(TextFeeds);
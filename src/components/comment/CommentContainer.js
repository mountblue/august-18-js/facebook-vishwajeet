import React, { Component } from 'react';
import { Timeline } from 'antd';
import CommentItem from './CommentItem';
import '../../App.css';
import ReplyToComment from './ReplyToComment';

class CommentsContainer extends Component {
  render() {
    return (
      <div>
      <h2>
Comments(
        {this.props.comment.length}
)
      </h2>
      {this.props.comment.length ? (
        <Timeline>
          {this.props.comment.map((comments) => (
            <CommentItem
              key={Math.random()}
             {...comments}
            />
          ))}
        </Timeline>
      ) : 'No comments'}
      <ReplyToComment id={this.props.id}/>
      </div>
      
    );
  }
}

export default CommentsContainer;
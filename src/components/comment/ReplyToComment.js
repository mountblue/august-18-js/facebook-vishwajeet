import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Form, Input, Icon } from 'antd';
import '../../App.css';
import { AddComment } from '../../actions/PostAction';

class ReplyToComment extends Component {
  onSubmit(e) {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        this.props.AddComment(values,this.props.id);
        this.props.form.resetFields();
      }
    });
  }

  render() {
    const { getFieldDecorator } = this.props.form;
    return (
      <Form layout="inline" onSubmit={e => this.onSubmit(e)}>
                <Form.Item label="" hasFeedback>
          {getFieldDecorator('comment', {
            rules: [{ required: true, message: 'Please enter your comment' }],
          })(
            <Input
              prefix={<Icon type="message" theme="outlined" style={{ color: 'rgba(0,0,0,.25)' }} />}
              type="textarea"
              placeholder="Reply to comment"
              rows={3}
            />,
          )}
        </Form.Item>
       
      </Form>
    );
  }
}
export default connect(null, { AddComment })(Form.create()(ReplyToComment));
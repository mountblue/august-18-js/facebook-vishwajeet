import React from 'react';
import { Timeline } from 'antd';
import '../../App.css';

const CommentItem = props => (
  <Timeline.Item>
     
    {props.comment}
   
  </Timeline.Item>
);

export default (CommentItem);

import {POST, COMMENT,LIKE } from '../actions/type';

const initialState= {
    items:[],
    Likes:[],
}
export default function(state = initialState,action){
    switch(action.type){
         case POST:
         return{
             ...state,
             items:action.payload
         }

         case LIKE:
        if(state.Likes.includes(action.payload.id)){
            const Likes = state.Likes.filter(data => data !== action.payload.id)
            const update = state.items.map(data => data);
            update[action.payload.id].likes--;
            return{
                ...state,
                Likes,
                items:update
            }
         }
         else{
            state.Likes.push(action.payload.id)
            const update = state.items.map(data => data);
            update[action.payload.id].likes++;
            return{
                ...state,
                items:update
            }
        }

         case COMMENT:
         const update = state.items.map(data => data);
         update[action.payload.id].comments.push(action.payload.data)
         return{
             ...state,
             items:update
         }

        default:
        return state;
    }
}